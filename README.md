# Help Bjorn Live Long
### How I find the approaches
When I read the assignment, I decide to firstly focus on the **dev** side, where the core feature is "similar", and then I can spend time fulfilling the end-to-end solution, which is the **ops** side considering scalability, reusablity, security, testability, etc.

So I started to research the "similar" feature, which requires algorithms to compare image difference. First researched on google with keyterms "image similarity algorithm", "compare image similarity" etc, found the below options/approaches:

1. Image Similarity API, such as
    * DeepAI
    * RapidAPI
    * Amazon Rekognition
2. Algorithms
    * Mean structural similarity - OpenCV or ScikitLearn skimage.measure.compare_ssim()
    * Perceptual Hash
    * Image hash algorithms
3. Machine Learning (Transfer learning)
    * Train a model over a large dataset using nets such as CNN, or select a pre-trained model. Then passing our image dataset through the neural nets to extract features, and use supervised learning such as KNN to find the similarity between "features"
   

### Clarification:
There is one thing that can cause confusion in the use case, which is the `path` in the csv

I noticed that "Each field contains the absolute path to an image file." There are two interpretations:
1. The paths are pointing to Bjorn’s local. 
2. The paths are pointing to a cloud storage. 

So there are scenarios:
1. Bjorn is using his mac to process csv pointing to a local folder
   * we can simply write a bash or python script and schedule a local cron job
2. Bjorn is using his windows to remote access his mac, file stays on mac
   * we can mount the local folder with minikube, and we need a reverse proxy or ingress like nginx or istio running in the local for his windows remote access
   * we can also deploy a https443/other protocal application with/without a UI for Bjorn to upload his folders at anytime and triggers the automation
   * we can fully use cloud trigger functionality, such as cloud Storage triggers + cloud functions
4. Bjorn is using his mac or windows to process csv pointing to a cloud storage
   * we can wrap the script as docker conatiner and deploy it as a kubernetes cronjob
   * we can configure a cloud pubsub notification for cloud storage, so everytime people updates the bucket, we can pull subscriptions from gcp and create our own logic
   * we can fully use cloud trigger functionality, such as cloud Storage triggers + cloud functions

Same question: Where is the csv located? I write a general input which can take relative paths or gsutil path.

### What if Bjorn wants to sync between local and cloud? (Quickly mock the datasets and environment)
[A short shell script](sync_bucket_job) has been written to quickly push local image sets (dataset/*.jpgs and gifs) into my personal gcp bucket.

This should be made into a cronjob with dockerfile and k8s deployment, due to time limitations I skip the easy part. 
It accepts one argument to specify the relative path. Sample code below:
```
cd ./sync_bucket_job
bash sync_bucket_job.sh ../dataset
```

### Performance

To avoid duplicated calculations between image pairs, we have the following solutions:
1. (Easy)We can dump the similarity_dict into a json and store that in the bucket as a cache. So that the next time we hit the same pair we can just look up the hashmap with O(1)
2. (Hard)If the csv is incremental only from the bottom, we can log a row index of last visited row in the csv every time we finish running.
3. Don't do caches. Since the filepath and filename might change due to business logics. We need to balance among security, data accuracy, infrastructure cost, time budget, etc

### Secrets
Secrets are stored in gitlab ci/cd variable and k8s secrets. It can be pushed by editing the template under `/secrets`


### Expose API
In this cronjob solution I don't have time to create an http server. With a backend server such as Django/Go/Node we can expose API for bjorn to interact with. K8s needs to be updated to provide LB/reverse proxy/gateway

### **Local Approach**
[mount the local folder with minikube and configure reverse proxy](scenarios/OnPrem/README.md)

### **Cloud Approach**
[Step-by-step guide](scenarios/Cloud/README.md)

### **Important Considerations**
* **How do you know if your code works?**
1. Testable. The solution has unit tests that make sure the job remains reliable after updates 
2. Scalable. The engine itself is running with docker images and kubernetes pods. By modifying the deployment script we can easily increase the distributed nodes and strategies for scalibility.
3. Extendable. Because of things are modulized and containerized, if we want to add a frontend, a gateway server, proxy server, etc, we can simply mount them with internal ports in k8s and expose only necessary ingress
4. Make friends with developers. I'm really open to answer and support any questions from Bjorn. Since this is just a POC, I will need to discuss with Bjorn on how we fit the solution into his environment. This is also a way to keep my code maintainable
      
* **How are you going to teach Bjorn how to use the program?**
   * **Architecture Diagram**
   
        Although I don't have time to draw a diagram. It's helpful to explain the folders and code with
   1. user flow diagram
   2. architecture diagram\
  to give Bjorn a quick big picture of how things stick together.
   * **Infrastructure**
     
     This cloud infrastructure was set using my own gcp account to mock his company infrastructure. 
   Assuming I'm building the project for him, he can use the service account I provided to interact with the cloud environment.
   For him to understand what's the infrastructure looks like and apply the IAC to his environment, he can refer to [/terraform/bjorns-hope](/terraform/bjorns-hope) and [/terraform/projects](/terraform/projects) and apply his terraform state.
   If he wants to simply import the states, he can use the Cloud Asset API:
        
            gcloud components install config-connector  #Install the gcloud CLI for Config Connector
            gcloud beta resource-config terraform generate-import ./terraform
      This will generate a shell script for him to import the terraform modules.
  
   * **An in-person walkthrough & Pair programming**
        
     I found it really helpful when I do job shadowing and pair programming. So I will take him to work on some tasks with me to help him understand the code.
   * **Good documentations and comments on code**
* **Your manager Jeanie is assigning you to a different task and is making Ferris the maintainer of your application. How do you make sure he succeeds?**
    
    Good documentation of current code, create tickets for the known bugs and TODOs, meetings and knowledge transfer session, good commenting and documentation, etc
* **How are you ensuring Bjorn gets the latest version of your application?**
    
    If Bjorn is a developer, he should be able to get the latest dev code from gitlab. If he only uses the cloud, the docker tagging and google container regristry can ensure that 
    he gets the latest version. Also, the cronjob has some syncing features to ensure the csvs and images are up-to-date
# Compromises
For this assignment I had a quite limited time since I had a badminton tournament on Saturday and basically I started working on Sunday morning.
I want to deliver a more thorough end-to-end solution with advanced tech stack and demonstrate my skill sets such as helm charts, ansible, istio sidecar etc. 
I also wanted to build a frontend server, an intermediate server or mq to process the event queue, more pods to be connected by the k8s yaml...
However, I did't have enough time to finish that. Helm charts are simplified to a single `K apply -f ` since I think it relatively low priority in this simple use case.


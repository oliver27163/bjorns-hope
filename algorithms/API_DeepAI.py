import base64

import requests

r = requests.post(
    "https://api.deepai.org/api/image-similarity",
    data={
        'image1': base64.b64encode(open('./dataset/0.jpg', 'rb').read()).decode('utf-8'),
        'image2': base64.b64encode(open('./dataset/1.jpg', 'rb').read()).decode('utf-8'),
    },
    headers={'api-key': 'quickstart-QUdJIGlzIGNvbWluZy4uLi4K'}
)
print(r.json())

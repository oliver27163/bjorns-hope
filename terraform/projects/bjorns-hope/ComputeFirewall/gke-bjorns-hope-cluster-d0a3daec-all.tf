resource "google_compute_firewall" "gke_bjorns_hope_cluster_d0a3daec_all" {
  allow {
    protocol = "ah"
  }

  allow {
    protocol = "esp"
  }

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "sctp"
  }

  allow {
    protocol = "tcp"
  }

  allow {
    protocol = "udp"
  }

  direction     = "INGRESS"
  name          = "gke-bjorns-hope-cluster-d0a3daec-all"
  network       = "https://www.googleapis.com/compute/v1/projects/bjorns-hope/global/networks/default"
  priority      = 1000
  project       = "bjorns-hope"
  source_ranges = ["10.48.0.0/14"]
  target_tags   = ["gke-bjorns-hope-cluster-d0a3daec-node"]
}
# terraform import google_compute_firewall.gke_bjorns_hope_cluster_d0a3daec_all projects/bjorns-hope/global/firewalls/gke-bjorns-hope-cluster-d0a3daec-all

import requests
import base64

with open("./dataset/0.jpg", "rb") as image_file:
    encoded_file_a = base64.b64encode(image_file.read()).decode('utf-8')
with open("./dataset/1.jpg", "rb") as image_file:
    encoded_file_b = base64.b64encode(image_file.read()).decode('utf-8')
url = "https://similarity2.p.rapidapi.com/similarity"

payload = {
    "image_a": {
        "type": "b64",
        "content": encoded_file_a
    },
    "image_b": {
        "type": "b64",
        "content": encoded_file_b
    }
}
headers = {
    "content-type": "application/json",
    "X-RapidAPI-Key": "a4fcffb13fmshfc8789d99763c7ep1e428fjsnbff7788d2b09",
    "X-RapidAPI-Host": "similarity2.p.rapidapi.com"
}

response = requests.request("POST", url, json=payload, headers=headers)

print(response.text)

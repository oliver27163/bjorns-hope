resource "google_compute_firewall" "gke_bjorns_hope_cluster_d0a3daec_inkubelet" {
  allow {
    ports    = ["10255"]
    protocol = "tcp"
  }

  direction     = "INGRESS"
  name          = "gke-bjorns-hope-cluster-d0a3daec-inkubelet"
  network       = "https://www.googleapis.com/compute/v1/projects/bjorns-hope/global/networks/default"
  priority      = 999
  project       = "bjorns-hope"
  source_ranges = ["10.48.0.0/14"]
  source_tags   = ["gke-bjorns-hope-cluster-d0a3daec-node"]
  target_tags   = ["gke-bjorns-hope-cluster-d0a3daec-node"]
}
# terraform import google_compute_firewall.gke_bjorns_hope_cluster_d0a3daec_inkubelet projects/bjorns-hope/global/firewalls/gke-bjorns-hope-cluster-d0a3daec-inkubelet

import math
import os
import pathlib
import time
import sys
from google.cloud import storage
from models.Output import Output
from utils.csv_helper import read_csv, write_csv
from algorithms.SSMI_OpenCV import compare_image_similarity, ImagePathType


def check_file_path_validity(filepath):
    try:
        if not isinstance(filepath, str) or not filepath:
            return False
        return os.path.exists(filepath)
    except OSError:
        return False


def detect_img_path_type(sample_image_pair):
    if sample_image_pair.image1.startswith('gs://'):
        return ImagePathType.GSUTIL_URL
    elif '/' in sample_image_pair.image1:
        return ImagePathType.LOCAL
    else:
        return ImagePathType.BLOB_NAME


def compute_image_pairs(image_pair_list):
    try:
        # Instance of google cloud storage client
        storage_client = storage.Client()
        # Use dict to prevent processing duplicate pairs
        similarity_dict = dict()
        output_list = list()
        # Check the csv contains which format of path: (Local | Gsutil | GCS blob name)
        img_path_type = detect_img_path_type(image_pair_list[0])
        # Loop through rows
        for index, image_pair in enumerate(image_pair_list):
            # If detected path type is local, check if the file exists
            if img_path_type == ImagePathType.LOCAL and (
                    not check_file_path_validity(image_pair.image1) or not check_file_path_validity(image_pair.image2)):
                raise Exception(
                    f"Safety Stop! Check if file exist or if the path is valid on row {index + 1} (header excluded)")
            # Check if result is already in the cache. If two file paths switch position, count it the same
            if f'{image_pair.image1}{image_pair.image2}' in similarity_dict:
                output_list.append(similarity_dict[f'{image_pair.image1}{image_pair.image2}'])
            elif f'{image_pair.image2}{image_pair.image1}' in similarity_dict:
                output_list.append(similarity_dict[f'{image_pair.image1}{image_pair.image2}'])
            else:
                # Cannot find the pair in the similarity_dict, start new comparison
                # Count start time
                start_time = time.time()
                # The same image provided in different formats, say, png and gif, should have a score of '0'
                similarity = math.trunc(0)
                # The formats are the same, start comparison
                if pathlib.Path(image_pair.image1).suffixes == pathlib.Path(image_pair.image2).suffixes:
                    # Round the result into 3 decimal places
                    similarity = round(
                        compare_image_similarity(img_path_type, image_pair.image1, image_pair.image2, storage_client),
                        3)
                # Mark time difference
                elapsed = round(time.time() - start_time, 3)
                result = Output(image_pair.image1, image_pair.image2, similarity, elapsed)
                # Save the result into a dict (count as hashmap) as a cache to save performance
                similarity_dict[f'{image_pair.image1}{image_pair.image2}'] = result
                output_list.append(result)
        return output_list
    except Exception as e:
        raise e


def generate_output_csv(output_list):
    try:
        write_csv(output_list)
    except Exception as e:
        raise e


def main():
    try:
        if len(sys.argv) > 1:
            csv_path = sys.argv[1]
            image_pair_list = read_csv(os.path.join(os.path.dirname(__file__), csv_path))
            output_list = compute_image_pairs(image_pair_list)
            generate_output_csv(output_list)
            print("Job succeeded!")
    except Exception as e:
        print(f'ERROR: {e}')
        raise e


if __name__ == '__main__':
    os.environ['SA_CRENDENTIAL'] = "gcp.json"
    main()

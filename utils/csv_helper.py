import csv
from datetime import date

from models.Input import ImagePair


def detect_delimiter(filename: str):
    """Detect delimiter from the csv, since excel uses space by default and other delimiters like comma are commenly used
    :param filename:
    :return: delimiter string
    """
    try:
        with open(filename) as f:
            sample_lines = [next(f).rstrip() for x in range(2)]
        possible_delimiters = [' ', ',', '\t', '|']
        for delimiter in possible_delimiters:
            if sample_lines[0].count(delimiter) > 0:
                if all([sample_lines[0].count(delimiter) == sample_lines[1].count(delimiter) for i in range(1, 2)]):
                    return delimiter
        return ','
    except Exception as e:
        raise e


def read_csv(file_name: str):
    """read csv by auto-detecting its delimiter
    :param file_name:
    :return: delimiter string
    """
    try:
        image_pair_list = list()
        with open(file_name, newline='') as csvfile:
            delimiter = detect_delimiter(file_name)
            reader = csv.reader(csvfile, delimiter=delimiter)
            header = next(reader, None)  # Skip the header.
            if len(header) == 2:  # Check that we have the correct input
                for image1, image2 in reader:
                    # Create the input instance and append it to the list.
                    image_pair_list.append(ImagePair(image1, image2))
            return image_pair_list
    except Exception as e:
        raise e


def write_csv(output_list):
    with open(f"output-{date.today()}.csv", "wt") as fp:
        writer = csv.writer(fp, delimiter=",")
        writer.writerow(output_list[0].to_key_list())
        for row_obj in output_list:
            writer.writerow(row_obj.to_value_list())

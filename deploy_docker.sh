#!/bin/bash

if $(docker --version | grep podman); then
	fmt_arg="--format=docker"
else
	fmt_arg=
fi;

echo "pushing project container image"
cd scenarios/Cloud

# Known compatibility issue
# Since I'm using a M1 chip macbook, the docker build will be arm64 instead of AMD64
# Build for ARM64
# docker build --platform=linux/arm64 -t <image-name>:<version>-arm64 .

# Build for AMD64
# docker build --platform=linux/amd64 -t <image-name>:<version>-amd64 .


docker compose build --pull
docker push $fmt_arg gcr.io/bjorns-hope/bjorns-hope:latest
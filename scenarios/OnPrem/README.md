## On Premise Approach to help Bjorn live long

### Install minikube on his macOS host
The prequesite to run k8s on his local is minikube, he will need to install it:
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
$ sudo install minikube-linux-amd64 /usr/local/bin/minikube
```
If his mac is using apple M1/M2 chip, use the below command:
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-darwin-arm64
$ sudo install minikube-darwin-arm64 /usr/local/bin/minikube
```
He can then start local k8s by:
```
minikube start —-driver=none
```
Then we need to configure nginx:
```
mkdir -p /etc/nginx/conf.d/ /etc/nginx/certs
cat /etc/nginx/conf.d/minikube.conf
```
...
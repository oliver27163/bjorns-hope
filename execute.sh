#!/bin/bash

# set service account inpersonation
gcloud auth activate-service-account --key-file=./sa.json
export GOOGLE_APPLICATION_CREDENTIALS="./sa.json"
gcloud config set auth/impersonate_service_account bjorn-sa@bjorns-hope.iam.gserviceaccount.com

if [ "$#" -eq 1 ]; then
  # use passed local path
  file_name="$1"
else
  # import from bucket
  echo "Importing csv from bucket..."
  /root/google-cloud-sdk/bin/gsutil ls gs://bjorns-hope-csv/*.csv >csv_files.txt
  while read file_name; do
    /root/google-cloud-sdk/bin/gsutil -m cp -r ${file_name} ./assets
    if [ $? -ne 0 ]; then
      exit 1
    fi
  done <csv_files.txt
  echo "Import completed"
fi

python3 main.py $file_name

if [ $? -ne 0 ]; then
  echo "Error exiting"
  exit 1
fi

echo "Transferring output csv to gcs bucket..."
/root/google-cloud-sdk/bin/gsutil mv ./output.csv gs://bjorns-hope-outputs

echo "Transfer done. Enjoy"

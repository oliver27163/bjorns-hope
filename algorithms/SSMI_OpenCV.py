import base64
from enum import Enum
from google.cloud import storage
from skimage.metrics import structural_similarity as compare_ssim
import argparse
import imutils
import cv2


class ImagePathType(Enum):
    LOCAL = 1
    GSUTIL_URL = 2
    BLOB_NAME = 3


def compare_image_similarity(img_path_type, image_a, image_b, storage_client):
    try:
        if img_path_type == ImagePathType.LOCAL:
            image_a_read = cv2.imread(image_a)
            image_b_read = cv2.imread(image_b)
        else:
            # connect to the gcp bucket
            bucket = storage_client.get_bucket('bjorns-hope-images')

            # download the files and read into openCV
            blob_a = bucket.blob(image_a)
            blob_a.download_to_filename(f'./dataset/{image_a}')
            image_a_read = cv2.imread(f'./dataset/{image_a}')

            blob_b = bucket.blob(image_b)
            blob_b.download_to_filename(f'./dataset/{image_b}')
            image_b_read = cv2.imread(f'./dataset/{image_b}')

            # Another solution, use gcsfs to open the binary file
            # # gcs_fs = gcsfs.GCSFileSystem(project='bjorns-hope')
            # with gcs_fs.open(f'bjorns-hope-images/{image_a}', 'rb') as f:
            #     image_a = cv2.imread(base64.b64encode(f.read()).decode('utf-8'))
            # with gcs_fs.open(f'bjorns-hope-images/{image_b}', 'rb') as f:
            #     image_b = cv2.imread(base64.b64encode(f.read()).decode('utf-8'))

        # Convert the images to grayscale
        img_a_gray = cv2.cvtColor(image_a_read, cv2.COLOR_BGR2GRAY)
        img_b_gray = cv2.cvtColor(image_b_read, cv2.COLOR_BGR2GRAY)

        # Calculate the SSIM between the images
        (score, diff) = compare_ssim(img_a_gray, img_b_gray, full=True)
        return score
    except Exception as e:
        raise e

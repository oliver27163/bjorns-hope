#!/bin/bash

minikube_installed=false
nginx_installed=false
if which minikube >/dev/null; then
  minikube_installed=true
else
  echo "Minikube is not installed"
fi

if which nginx >/dev/null; then
  nginx_installed=true
else
  echo "Nginx is not installed"
fi

echo $minikube_installed
resource "google_compute_firewall" "gke_bjorns_hope_cluster_d0a3daec_exkubelet" {
  deny {
    ports    = ["10255"]
    protocol = "tcp"
  }

  direction     = "INGRESS"
  name          = "gke-bjorns-hope-cluster-d0a3daec-exkubelet"
  network       = "https://www.googleapis.com/compute/v1/projects/bjorns-hope/global/networks/default"
  priority      = 1000
  project       = "bjorns-hope"
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["gke-bjorns-hope-cluster-d0a3daec-node"]
}
# terraform import google_compute_firewall.gke_bjorns_hope_cluster_d0a3daec_exkubelet projects/bjorns-hope/global/firewalls/gke-bjorns-hope-cluster-d0a3daec-exkubelet

import unittest

from algorithms.SSMI_OpenCV import compare_image_similarity, ImagePathType
from google.cloud import storage
from models.Input import ImagePair
from models.Output import Output
from utils.csv_helper import read_csv, detect_delimiter
from main import check_file_path_validity, detect_img_path_type, compute_image_pairs


class Tests(unittest.TestCase):
    def test_compare_image_similarity_local_1(self):
        mock_client = storage.Client()
        result = compare_image_similarity(ImagePathType.LOCAL, './tests/0.jpg', './tests/1.jpg', mock_client)
        self.assertEqual(result, 0.11341004743026814)

    def test_compare_image_similarity_local_2(self):
        mock_client = storage.Client()
        result = compare_image_similarity(ImagePathType.LOCAL, './tests/2.jpg', './tests/3.jpg', mock_client)
        self.assertEqual(result, 0.21597910657455613)

    def test_compare_image_similarity_local_identical(self):
        mock_client = storage.Client()
        result = compare_image_similarity(ImagePathType.LOCAL, './tests/1.jpg', './tests/1.jpg', mock_client)
        self.assertEqual(result, 1)

    def test_read_csv(self):
        result = read_csv('./tests/test_csv.csv')
        self.assertEqual(len(result), 5)
        self.assertEqual(result[0].image1, '0.jpg')
        self.assertEqual(result[0].image2, '1.jpg')
        self.assertEqual(result[1].image1, '2.jpg')
        self.assertEqual(result[1].image2, '3.jpg')

    def test_detect_delimiter(self):
        result = detect_delimiter('./tests/test_csv.csv')
        self.assertEqual(result, ',')

    def test_detect_delimiter2(self):
        result = detect_delimiter('./tests/test_csv2.csv')
        self.assertEqual(result, ' ')

    def test_check_file_path_validity(self):
        result = check_file_path_validity('./tests/test_csv.csv')
        self.assertEqual(result, True)
        result = check_file_path_validity('./tests/test_csv3.csv')
        self.assertEqual(result, False)

    def test_detect_img_path_type(self):
        result = detect_img_path_type(ImagePair('gs://bjorns-hope-images/6.jpg', 'gs://bjorns-hope-images/1.jpg'))
        self.assertEqual(result, ImagePathType.GSUTIL_URL)
        result = detect_img_path_type(ImagePair('0.jpg', '1.jpg'))
        self.assertEqual(result, ImagePathType.BLOB_NAME)
        result = detect_img_path_type(ImagePair('./tests/0.jpg', './tests/1.jpg'))
        self.assertEqual(result, ImagePathType.LOCAL)

    def test_compute_image_pairs(self):
        dummy = [ImagePair('./tests/0.jpg', './tests/1.jpg'), ImagePair('./tests/2.jpg', './tests/3.jpg')]
        result = compute_image_pairs(dummy)
        self.assertEqual(result[0].image1, './tests/0.jpg')
        self.assertEqual(result[0].image2, './tests/1.jpg')
        self.assertEqual(result[0].similarity, 0.113)
        self.assertEqual(result[1].image1, './tests/2.jpg')
        self.assertEqual(result[1].image2, './tests/3.jpg')
        self.assertEqual(result[1].similarity, 0.216)


if __name__ == '__main__':
    unittest.main()

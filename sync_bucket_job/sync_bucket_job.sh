#!/bin/bash

lockfile="./syncBucketJob.lock"

if [ -f $lockfile ]; then
  echo "Job already running. Exiting."
  exit 1
else
  mkdir -p $lockfile
  ls $1/* >image_files.txt

  if [ ! -s image_files.txt ]; then
    echo "No files found. Exiting."
    rmdir $lockfile
    exit 1
  fi

  while read file; do
    gsutil -m cp -r ${file} gs://bjorns-hope-images
    if [ $? -ne 0 ]; then
      echo "Error copying file"
      rmdir $lockfile
      exit 1
    fi
  done <image_files.txt
  rmdir $lockfile
  exit 0
fi

resource "google_service_account" "bjorn_sa" {
  account_id   = "bjorn-sa"
  display_name = "bjorn-sa"
  project      = "bjorns-hope"
}
# terraform import google_service_account.bjorn_sa projects/bjorns-hope/serviceAccounts/bjorn-sa@bjorns-hope.iam.gserviceaccount.com

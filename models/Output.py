from decimal import Decimal


class Output:
    # float can cause 13.949999999999999 problem, Decimal is a better present for roundings
    def __init__(self, image1: str, image2: str, similarity: Decimal, elapsed: Decimal):
        self.image1 = image1
        self.image2 = image2
        self.similarity = similarity
        self.elapsed = elapsed

    def to_key_list(self):
        return self.__dict__.keys()

    def to_value_list(self):
        return [self.image1, self.image2, self.similarity, self.elapsed]

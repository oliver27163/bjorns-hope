# Bjorn's Hope (main solution)

This solution is utilizing python to create a script that adaptively reads from csv (auto-detect formats and delimiters, auto-detect where the image paths are pointing to (local | cloud url | cloud blob name)), 
and periodically run in kubernetes workloads as a cronjob to generate the output csv.

### Folder structure explanation:
`/algorithms` lists a few different algos/approaches. Used a script in my local to compare them and figured out the best fit one with SSMI (easy to prove compared with ML, and better accuracy compared with custom algo) \
`/deployment` contains the kubernetes deployment configuration files, and a helper script to add a namespace for a new gcp project. \
`/models` contains the custom defined python object \
`/scenarios` was planned to give other solutions such as minikube+nginx. [But due to time limitations I will only explain how it works.](../../README.md/#Compromises) \
`/secrets` is the template folder to update service account credentials in kubernetes secrets. **In theory and production the sa should never be exposed, but since you can't view my gcp and gitlab ci/cd variables, I'm leaving the sa for my personal gcp** \
`/terraform` has the current terraform state and modules of my personal gcp. This is for Bjorn's reference \
`/tests` contains the test code for python and gitlab cicd pipelines \
`/utils` contains helper functions


The local case assumes that: \
`/assets` contains the input csvs  \
`/dataset` contains the images which the paths in input csvs point to, if not it will download the *.csv from the gcs bucket.
`/sync_bucket_job` contains the script to upload local images to bucket so it can be run in the cloud


### GCP explanation:
This poc is running on my own gcp with project name bjorns-hope.

The authentication for Bjorn can go through service account inpersonation. This project uses bjorn-sa@bjorns-hope.iam.gserviceaccount.com.


The bucket looks like this (this can be improved to folder structure bjorns-hope/*)
![img.png](bucket_screenshot.png)


### Build
There's a build stage in gitlab CI, the pipeline will build the docker image, tag it and push it to gcr.io in my personal gcp.git

If you need to build the docker image from local, run:
```
cd ./scenarios/Cloud
docker compose build --pull
```

To trigger the container once with test csvs, run:
```
# local_paths has local relative path in the csv
docker run gcr.io/bjorns-hope/bjorns-hope:latest ./assets/local_paths.csv
# blob_names is the image name in gcs bucket
docker run gcr.io/bjorns-hope/bjorns-hope:latest ./assets/blob_names.csv
```

To one-click build the image and push to the remote gcr, run:
```
bash ./deploy_docker.sh
```

To deploy the cron job to kubernetes, run:
```
cd ./deployment
kubectl apply -f deployment-k8s.yaml --namespace=bjorns-hope-mars
```
### Tests
There's a test stage in gitlab ci, you can also manually trigger it by:
`python3 -m unittest discover`

### <del>Known compatibility issue
<del>Since when building on an M1 chip macbook, the image will have platform arm64 instead of AMD64.
If you're building from local, for docker build you can use:</del>
```
# Build for ARM64 
docker build --platform=linux/arm64 -t <image-name>:<version>-arm64 .

# Build for AMD64
docker build --platform=linux/amd64 -t <image-name>:<version>-amd64 .
```
<del>For docker compose you can set: \
```export DOCKER_DEFAULT_PLATFORM=linux/amd64```</del>

The k8s architecture can be found by:
`kubectl describe nodes`\
This have been fixed by adding `platform` attribute to docker-compose.yml and --platform=$BUILDPLATFORM check in dockerfile

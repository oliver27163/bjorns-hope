resource "google_artifact_registry_repository" "bjorns_hope" {
  format        = "DOCKER"
  location      = "us-east1"
  project       = "bjorns-hope"
  repository_id = "bjorns-hope"
}
# terraform import google_artifact_registry_repository.bjorns_hope projects/bjorns-hope/locations/us-east1/repositories/bjorns-hope

resource "google_storage_bucket" "artifacts_bjorns_hope_appspot_com" {
  force_destroy            = false
  location                 = "US"
  name                     = "artifacts.bjorns-hope.appspot.com"
  project                  = "bjorns-hope"
  public_access_prevention = "inherited"
  storage_class            = "STANDARD"
}
# terraform import google_storage_bucket.artifacts_bjorns_hope_appspot_com artifacts.bjorns-hope.appspot.com
